import React, { useEffect, useState } from 'react';
import './App.css';
import CartDemo from './components/CartDemo';


function App() {
  return (
    <div className="App">
      <CartDemo/>
    </div>
  );
}

export default App;
