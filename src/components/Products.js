

const Products = props => {

    return (
        <>
        <header><strong>Add Products to Cart</strong></header>
        <ul>
            {props.products && props.products.map(product => (
                <li key={product.id}>
                    {product.name}
                    <button onClick={props.onClick.bind(this, product)}>Add to Cart</button>
                </li>
            ))}
        </ul>
        </>
    )
}

export default Products;
