import React from "react";

const Carts = props => {

    return (
        <>
            <header>Products in Cart</header>
            <ul>
                 {props.cartCount > 0 && props.carts.map(cart => (
                    <li key={cart.id}>
                        <strong>{cart.name}</strong>({cart.quantity})  ${cart.price * cart.quantity}
                        <button onClick={props.onClick.bind(this, cart)}>Remove From Cart</button>
                    </li>
                ))}
            </ul>
        </>
    );
}

export default Carts;
