import React, { useEffect, useState } from "react";
import Carts from "./Carts";
import Products from "./Products";

const CartDemo = () => {
   const [products, setProducts] = useState([]);
   const [carts, setCarts] = useState([]);
   const [count, setCount] = useState(0);

    useEffect(() => {
        let items = [{
            id: '1',
            name: 'Product 1',
            price: 20,
            quantity: 0
        },
        {
            id: '2',
            name: 'Product 2',
            price: 30,
            quantity: 0
        }];
        setProducts(items);
    }, []);

    const addToCart = product => {
        const updatedCarts = carts;
        const cartIndex = updatedCarts.findIndex(item => item.id === product.id);

        if (cartIndex === -1) {
            updatedCarts.push({...product, quantity: 1});
        } else {
            const updatedItem = updatedCarts[cartIndex];
            updatedItem.quantity++;
            updatedCarts[cartIndex] = updatedItem;
        }
        setCarts(updatedCarts);
        setCount(count + 1);
    };

    const removeFromCart = cart => {
        const updatedCarts = carts;
        const cartIndex = updatedCarts.findIndex(item => item.id === cart.id);

        const updatedItem = updatedCarts[cartIndex];
        updatedItem.quantity--;

        if (updatedItem.quantity === 0) {
            updatedCarts.splice(updatedItem, 1);
        } else {
            updatedCarts[cartIndex] = updatedItem;
        }

        setCarts(updatedCarts);
        setCount(count - 1);
    }

    return (
        <>
        <Products products = {products} onClick = {addToCart}></Products>
        <Carts carts = {carts} cartCount = {count} onClick = {removeFromCart}></Carts>

        </>
    )


}

export default CartDemo;
